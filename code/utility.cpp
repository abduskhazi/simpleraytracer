#include <iostream>
#include <cmath>
#include "Model3D.h"
#include "Image.h"
#include "Ray.h"

using namespace std;

constexpr double PI = 3.14159265;

//============================================================================================
//                                   GENERAL
//============================================================================================

Vertex cross_product(Vector3D const& A, Vector3D const& B)
{
    return {
        A.y * B.z - A.z * B.y,
        A.z * B.x - A.x * B.z,
        A.x * B.y - A.y * B.x
    };
}

double dot_product(Vector3D const& A, Vector3D const& B)
{
    return A.x * B.x + A.y * B.y + A.z * B.z;
}

Vector3D normalized(Vector3D const& vec)
{
    auto x = vec.x;
    auto y = vec.y;
    auto z = vec.z;
    auto vec_length = sqrt(x*x + y*y + z*z);

    return Vector3D{
        x/vec_length,
        y/vec_length,
        z/vec_length
    };
}

Intersection intersect(Ray const& r, Triangle const& t)
{
    Intersection intersect;

    auto e1 = t.p1 - t.p0;
    auto e2 = t.p2 - t.p0;
    auto s = r._origin - t.p0;
    auto d = r._direction;

    auto d_cross_e2 = cross_product(d,e2);
    auto s_cross_e1 = cross_product(s,e1);

    auto d_cross_e2_dot_e1 = dot_product(d_cross_e2, e1);
    if(d_cross_e2_dot_e1 != 0)
    {
        intersect.t = dot_product(s_cross_e1, e2) / d_cross_e2_dot_e1;
        intersect.b1 = dot_product(d_cross_e2, s) / d_cross_e2_dot_e1;
        intersect.b2 = dot_product(s_cross_e1, d) / d_cross_e2_dot_e1;
        intersect.normal = normalized(cross_product(e1, e2));
    }

    return intersect;
}

Pixel apply_phong_illumination(Vector3D const& normal, Light const& light, Direction const& v, Pixel const& object_color, double beta, double gamma, double m)
{
    auto light_color = light._color;

    auto object_color_vector = ColorVector{
                                1.0 * object_color.R / 255,
                                1.0 * object_color.G / 255,
                                1.0 * object_color.B / 255
                              };

    auto light_color_vector = ColorVector{
                                1.0 * light_color.R / 255,
                                1.0 * light_color.G / 255,
                                1.0 * light_color.B / 255
                              };

    auto white_color_vector = ColorVector{
                                1.0,
                                1.0,
                                1.0
                              };

    //Assuming the vectors are normalized here.
    double dot_product_result = dot_product(normal, light._direction);
    double reflected_component = dot_product_result > 0.0 ? dot_product_result : 0.0;
    auto diffuse_reflection = 1.0 / PI * reflected_component * (object_color_vector * light_color_vector);
    auto pixel_color_vector = beta * diffuse_reflection;

    if(reflected_component > 0)
    {
        auto r = 2 * reflected_component * normal - light._direction;
        auto specular_component = dot_product(r, -1 * v);
        if(specular_component >= 0.0)
        {
            auto specular_reflection = reflected_component * pow(specular_component, m) * light_color_vector * light_color_vector;
            pixel_color_vector = pixel_color_vector + gamma * specular_reflection;
        }
    }

    pixel_color_vector.x = min(pixel_color_vector.x, 1.0);
    pixel_color_vector.y = min(pixel_color_vector.y, 1.0);
    pixel_color_vector.z = min(pixel_color_vector.z, 1.0);

    pixel_color_vector =  255 * pixel_color_vector;
    return Pixel{
            (uint8_t)pixel_color_vector.x,
            (uint8_t)pixel_color_vector.y,
            (uint8_t)pixel_color_vector.z
         };
}

bool Intersection_comparison(Intersection const& a, Intersection const& b)
{
    return a.t < b.t;
}

void Model3D::createBoundingBox()
{
    for(auto const& v : _vertices)
    {
        _bbox.update(v);
    }
}

void capture_image(Image& img, CameraViewPlane const& camera_view_plane, Model3D const& model, Light const& light, Pixel const& object_color, double beta, double gamma, int m)
{
    //Assuming that the camera viewpoint is at 0,0,0
    //Plane of view is parallel to the z plane.
    auto ray_vec = camera_view_plane.left_top - Vertex{0,0,0,1};
    auto horizontal_vector = camera_view_plane.get_horizontal_vector();
    auto vertical_vector = camera_view_plane.get_vertical_vector();
    for(size_t h = 0 ; h < img.height(); ++h)
    {
        for(size_t w = 0; w < img.width(); ++w)
        {
            double w_scale = w * 1.0 / img.width();
            double h_scale = h * 1.0 / img.height();
            auto ray = ray_vec + w_scale*horizontal_vector + h_scale*vertical_vector;

            auto r = Ray(Vertex{0,0,0,1} + ray, normalized(ray)); //Ray({0,0,0}, {0,0,-3});
            auto viewer_direction = r._direction;
            auto intersection_list = model.getIntersectionList(r);
            if(intersection_list.size() > 0)
            {
                sort(intersection_list.begin(), intersection_list.end(), Intersection_comparison);
                auto closest_intersection = intersection_list[0];
                img[h][w] = apply_phong_illumination(closest_intersection.normal, light, viewer_direction, object_color, beta, gamma, m);
            }
        }
    }
}

//============================================================================================
//                                   FACE
//============================================================================================

ostream& operator<<(ostream& os, const Point& p)
{
    if(p.index > 0)
        os << p.index;
    os << "/";

    if(p.texture_index > 0)
        os << p.texture_index;
    os << "/";

    if(p.normal_index)
        os << p.normal_index;

    return os;
}

istream& operator>>(istream& is, Point& p)
{
    auto s = string{};

    getline(is, s, '/');
    stringstream(s) >> p.index;

    s = string{};
    getline(is, s, '/');
    stringstream(s) >> p.texture_index;

    s = string{};
    getline(is, s, '/');
    stringstream(s) >> p.normal_index;

    return is;
}

ostream& operator<<(ostream& os, const Face& f)
{
    os << "f ";
    os << f.p1 << " ";
    os << f.p2 << " ";
    os << f.p3;
    return os;
}

istream& operator>>(istream& is, Face& f)
{
    string point1;
    string point2;
    string point3;
    is >> point1 >> point2 >> point3;

    stringstream(point1) >> f.p1;
    stringstream(point2) >> f.p2;
    stringstream(point3) >> f.p3;

    return is;
}

//============================================================================================
//                                   Model3D
//============================================================================================

Model3D::Model3D(string const& obj_file_name)
{
    //pushing a dummy vertex as the indices in the object
    //file start from 1. - Should we do this?
    //_vertices.push_back(Vertex{-1,-1,-1});
    
    ifstream file(obj_file_name);

    if (not file.is_open()) {
        cerr << "Could not open file '" << obj_file_name << "'" << endl;
        throw std::exception();
    }
    
    for(string line; getline(file, line); )
    {
        stringstream ss(line);
        string first_word;
        ss >> first_word;
        if(first_word == "v")
        {
            Vertex v;
            ss >> v;
            _vertices.push_back(v);
        }
        if(first_word == "vn")
        {
            VertexNormal vn;
            ss >> vn;
            _normals.push_back(vn);
        }
        else if(first_word == "f")
        {
            Face f;
            ss >> f;
            _faces.push_back(f);
        }
    }
}

void Model3D::addLight(Light const& light, double dist_adjustment)
{
    _light = light;

    auto w = 0.05;
    auto h = 0.05;

    auto right_bottom = Vertex{w,0,-h,1} + dist_adjustment * light._direction;
    auto right_top = Vertex{w,0,h,1} + dist_adjustment * light._direction;
    auto left_bottom = Vertex{-w,0,-h,1} + dist_adjustment * light._direction;

    _vertices.push_back(right_bottom);
    _vertices.push_back(right_top);
    _vertices.push_back(left_bottom);

    int last_index = static_cast<int>(_vertices.size());

    Face f;

    f.p1.index = last_index - 2;
    f.p2.index = last_index - 1;
    f.p3.index = last_index;
    _faces.push_back(f);

    f.p1.index = last_index;
    f.p2.index = last_index - 1;
    f.p3.index = last_index - 2;
    _faces.push_back(f);
}

// 4*4 matrix for homogenous notation.
vector<vector<double>> getIdentityMatrix()
{
    size_t dim = 4;
    vector<vector<double>> I;
    for(size_t i = 0 ; i < dim ; ++i)
    {
        I.push_back(vector<double>{});
        for(size_t j = 0 ; j < dim ; ++j)
        {
            I[i].push_back(0.0);
        }
        I[i][i] = 1.0;
    }

    return I;
}

Vertex operator*(vector<vector<double>> const& Matrix, Vertex const& v)
{
    return Vertex{
        Matrix[0][0]*v.x + Matrix[0][1]*v.y + Matrix[0][2]*v.z + Matrix[0][3]*v.t,
        Matrix[1][0]*v.x + Matrix[1][1]*v.y + Matrix[1][2]*v.z + Matrix[1][3]*v.t,
        Matrix[2][0]*v.x + Matrix[2][1]*v.y + Matrix[2][2]*v.z + Matrix[2][3]*v.t,
        Matrix[3][0]*v.x + Matrix[3][1]*v.y + Matrix[3][2]*v.z + Matrix[3][3]*v.t
    };
}

vector<vector<double>> get_Rx(double angle)
{
    angle = angle * PI / 180.0;

    auto Rx = getIdentityMatrix();
    Rx[1][1] = cos(angle);
    Rx[1][2] = -sin(angle);
    Rx[2][1] = sin(angle);
    Rx[2][2] = cos(angle);

    return Rx;
}

vector<vector<double>> get_Ry(double angle)
{
    angle = angle * PI / 180.0;

    auto Ry = getIdentityMatrix();
    Ry[0][0] = cos(angle);
    Ry[0][2] = sin(angle);
    Ry[2][0] = -sin(angle);
    Ry[2][2] = cos(angle);

    return Ry;
}

vector<vector<double>> get_Rz(double angle)
{
    angle = angle * PI / 180.0;

    auto Rz = getIdentityMatrix();
    Rz[0][0] = cos(angle);
    Rz[0][1] = -sin(angle);
    Rz[1][0] = sin(angle);
    Rz[1][1] = cos(angle);

    return Rz;
}

void Model3D::applyTransformation(vector<vector<double>> const& tranformationMatrix)
{
    for(auto& _v : _vertices)
    {
        _v = tranformationMatrix * _v;
    }
    _light._direction = tranformationMatrix * _light._direction;
}

void Model3D::translate(Vector3D const& vec)
{
    // Create the 4*4 homogenous matrix for translation
    auto translationMatrix = getIdentityMatrix();
    translationMatrix[0][3] = vec.x;
    translationMatrix[1][3] = vec.y;
    translationMatrix[2][3] = vec.z;

    this->applyTransformation(translationMatrix);
}

void Model3D::rotateX(double angle_along_x_axis)
{
    // Anticlockwise rotation with respect to object.
    // Clockwise rotation with respect to surroundings.
    auto Rx = get_Rx(angle_along_x_axis);
    this->applyTransformation(Rx);
}

void Model3D::rotateY(double angle_along_y_axis)
{
    // Anticlockwise rotation with respect to object.
    // Clockwise rotation with respect to surroundings.
    auto Ry = get_Ry(angle_along_y_axis);
    this->applyTransformation(Ry);
}

void Model3D::rotateZ(double angle_along_z_axis)
{
    // Anticlockwise rotation with respect to object.
    // Clockwise rotation with respect to surroundings.
    auto Rz = get_Rz(angle_along_z_axis);
    this->applyTransformation(Rz);
}

ostream& operator<<(ostream& os, const Model3D& m)
{
    for(auto const& v: m._vertices)
        os << v << endl;
    for(auto const& vn: m._normals)
        os << vn << endl;
    for(auto const& f: m._faces)
        os << f << endl;
    return os;
}

vector<Triangle> Model3D::traingles() const
{
    vector<Triangle> vect_t;

    for(auto const& f: _faces)
    {
        auto p1 = _vertices[f.p1.index - 1];
        auto p2 = _vertices[f.p2.index - 1];
        auto p3 = _vertices[f.p3.index - 1];

        Triangle t{p1, p2, p3};
        vect_t.push_back(t);
    }

    return vect_t;
}

bool Model3D::intersectsBoundingBox(Ray const& r) const
{
    //Calculate intersections here
    double tx1 = (_bbox.x[0] - r._origin.x) / r._direction.x;
    double tx2 = (_bbox.x[1] - r._origin.x) / r._direction.x;
    double txMin = min(tx1, tx2);
    double txMax = max(tx1, tx2);

    double ty1 = (_bbox.y[0] - r._origin.y) / r._direction.y;
    double ty2 = (_bbox.y[1] - r._origin.y) / r._direction.y;
    double tyMin = min(ty1, ty2);
    double tyMax = max(ty1, ty2);

    double tz1 = (_bbox.z[0] - r._origin.z) / r._direction.z;
    double tz2 = (_bbox.z[1] - r._origin.z) / r._direction.z;
    double tzMin = min(tz1, tz2);
    double tzMax = max(tz1, tz2);

    float tmin = max(max(txMin, tyMin), tzMin);
    float tmax = min(min(txMax, tyMax), tzMax);

    //Return false if the ray does not intersect the bouding box.
    if ( tmin > tmax )
        return false;

    return true;
}

vector<Intersection> Model3D::getIntersectionList(Ray const& r) const
{
    vector<Intersection> intersection_list;

    // If the ray does not intersect the bounding box, it does not intersect
    // any the model
    if(not this->intersectsBoundingBox(r))
    {
        return intersection_list;
    }

    for(auto const& f: _faces)
    {
        auto p1 = _vertices[f.p1.index - 1];
        auto p2 = _vertices[f.p2.index - 1];
        auto p3 = _vertices[f.p3.index - 1];

        Triangle t{p1, p2, p3};
        auto intersection = intersect(r, t);
        if(intersection.isValid())
        {
            intersection_list.push_back(intersection);
        }
    }
    return intersection_list;
}

Model3D Model3D::intersectedModel(Ray const& r) const
{
    Model3D intersected_model;
    for(auto const& v: _vertices)
        intersected_model._vertices.push_back(v);
    for(auto const& vn: _normals)
        intersected_model._normals.push_back(vn);

    for(auto const& f: _faces)
    {
        auto p1 = _vertices[f.p1.index - 1];
        auto p2 = _vertices[f.p2.index - 1];
        auto p3 = _vertices[f.p3.index - 1];

        Triangle t{p1, p2, p3};
        auto intersection = intersect(r, t);
        if(intersection.isValid())
        {
            intersected_model._faces.push_back(f);
        }
    }

    return intersected_model;
}

void Model3D::addCameraPlane(double width, double height, double dist)
{
    auto w = width / 2;
    auto h = height / 2;

    _camera_view_plane.right_bottom = Vertex{w,-h,dist,1};
    _camera_view_plane.right_top = Vertex{w,h,dist,1};
    _camera_view_plane.left_top = Vertex{-w,h,dist,1};
    _camera_view_plane.left_bottom = Vertex{-w,-h,dist,1};

    _vertices.push_back(_camera_view_plane.right_bottom);
    _vertices.push_back(_camera_view_plane.right_top);
    _vertices.push_back(_camera_view_plane.left_top);
    _vertices.push_back(_camera_view_plane.left_bottom);

    int last_index = static_cast<int>(_vertices.size());

    Face f;
    f.p1.index = last_index - 3;
    f.p2.index = last_index - 2;
    f.p3.index = last_index - 1;
    _faces.push_back(f);

    f.p1.index = last_index - 1;
    f.p2.index = last_index - 2;
    f.p3.index = last_index - 3;
    _faces.push_back(f);

    f.p1.index = last_index - 3;
    f.p2.index = last_index - 1;
    f.p3.index = last_index;
    _faces.push_back(f);

    f.p1.index = last_index;
    f.p2.index = last_index - 1;
    f.p3.index = last_index - 3;
    _faces.push_back(f);
}

void Model3D::removeLightFacesAndVertices()
{
    // Remove only the faces that have been added to the model by light.
    _vertices.resize(_vertices.size() - 3);
    _faces.resize(_faces.size() - 2);
}

void Model3D::removeCameraPlaneFacesAndVertices()
{
    // Remove only the faces that have been added to the model by CameraPlane.
    _vertices.resize(_vertices.size() - 4);
    _faces.resize(_faces.size() - 4);
}
//============================================================================================
//                                   Image
//============================================================================================

ostream& operator<<(ostream& os, const Image& img)
{
    os << "P6" << "\n";
    os << img.width() << " " << img.height() << "\n";
    os << 255 << "\n";

    for(auto const& row : img._data)
        for(auto const& pixel : row)
            os << pixel.R << pixel.G << pixel.B;

    return os;
}

Image::Image(size_t width, size_t height, Pixel default_pixel)
{
    _width = width;
    _height = height;

    _data = vector<vector<Pixel>>(height);
    for(auto& vec : _data) {
        vec = vector<Pixel>(width, default_pixel);
    }
}

//============================================================================================
//                                   Ray
//============================================================================================
Ray::Ray(Vertex const& o, Direction const& d)
{
    _origin = o;
    _direction = d;
}

//============================================================================================
//                                   Vertex
//============================================================================================

Vertex operator-(Vertex const& v1 , Vertex const& v2)
{
    return {
            v1.x - v2.x,
            v1.y - v2.y,
            v1.z - v2.z,
            v1.t - v2.t
    };
}

Vertex operator+(Vertex const& v1 , Vertex const& v2)
{
    return {
            v1.x + v2.x,
            v1.y + v2.y,
            v1.z + v2.z,
            v1.t + v2.t
    };
}

Vertex operator*(double scale, Vertex const& v)
{
    return {
            scale * v.x,
            scale * v.y,
            scale * v.z,
            v.t
    };
}

Vertex operator*(Vertex const& v1 , Vertex const& v2)
{
    return {
            v1.x * v2.x,
            v1.y * v2.y,
            v1.z * v2.z
    };
}

ostream& operator<<(ostream& os, const Vertex& v)
{
    os << "v ";
    os << setprecision(7) << v.x << " ";
    os << setprecision(7) << v.y << " ";
    os << setprecision(7) << v.z;
    return os;
}

istream& operator>>(istream& is, Vertex& v)
{
    is >> v.x >> v.y >> v.z;
    v.t = 1; // For homogenous notation. As this is a Vertex.
    return is;
}

ostream& operator<<(ostream& os, const VertexNormal& vn)
{
    os << "vn ";
    os << setprecision(7) << vn.x << " ";
    os << setprecision(7) << vn.y << " ";
    os << setprecision(7) << vn.z;
    return os;
}

istream& operator>>(istream& is, VertexNormal& vn)
{
    is >> static_cast<Vertex&>(vn);
    vn.t = 0; // For homogenous notation. As this is a Vector.
    return is;
}
//============================================================================================
//                                   BACKUP IMPLEMENTATION FOR TESTING
//============================================================================================

#if 0
for(size_t h = 0 ; h < img.height(); ++h)
    for(size_t w = 0; w < img.width(); ++w)
        if(w>img.width()/1.5 and h<img.height()/3)
            img[h][w] = Pixel{255, 255, 255};
#endif
