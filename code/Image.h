#ifndef __IMAGE_H__
#define __IMAGE_H__

#include <vector>
#include <cstdint>

using namespace std;

struct Pixel
{
    uint8_t R;
    uint8_t G;
    uint8_t B;
};

class Image
{
  public:
    Image(size_t width, size_t height, Pixel default_pixel = {0, 0, 0});

    size_t width() const {
        return _width;
    }

    size_t height() const {
        return _height;
    }

    vector<Pixel>& operator[](size_t i) {
        return _data[i];
    }

    friend ostream& operator<<(ostream& os, const Image& img);

  private:
        vector<vector<Pixel>> _data;
        size_t _width;
        size_t _height;
};

#endif //__IMAGE_H__
