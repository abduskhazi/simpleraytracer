#ifndef __VERTEX_H__
#define __VERTEX_H__

#include <iostream>
#include <iomanip>
using namespace std;

struct Vertex
{
    double x = {};
    double y = {};
    double z = {};
    double t = {};
    
    friend ostream& operator<<(ostream& os, const Vertex& v);
    friend istream& operator>>(istream& is, Vertex& v);
};

struct VertexNormal : public Vertex
{
    friend ostream& operator<<(ostream& os, const VertexNormal& vn);
    friend istream& operator>>(istream& is, VertexNormal& vn);
};

Vertex operator-(Vertex const& v1 , Vertex const& v2);
Vertex operator+(Vertex const& v1 , Vertex const& v2);
Vertex operator*(Vertex const& v1 , Vertex const& v2);
Vertex operator*(double scale, Vertex const& v);

using Direction = Vertex;
using Vector3D = Vertex;
// R = x; G = y; B = z; t = unused.
using ColorVector = Vertex;

#endif //__VERTEX_H__
