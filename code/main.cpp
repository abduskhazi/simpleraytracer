// To compile the code on linux use the command
// g++ --std=c++17 main.cpp utility.cpp
// To run the code execute with a model name e.g. bunny.obj
// ./a.out bunny.obj

#include <iostream>
#include <fstream>
#include "Model3D.h"
#include "Image.h"
#include "Ray.h"

void capture_image(Image&, CameraViewPlane const&, Model3D const&, Light const&, Pixel const&, double, double, int);
Vector3D normalized(Vector3D const& vec);

using namespace std;

// ##############################################################################################################
//    PROJECT PARAMETERS - CHANGE THESE FOR CHANGING HOMOGENOUS TRANSFORMATIONS AND PHONG ILLUMINATION PARAMTERS
// ##############################################################################################################

// Angles defining the rotation of model. The values are in degrees.
const double x_axis_rotation = 0.0;
const double y_axis_rotation = 0.0;
const double z_axis_rotation = 0.0;

// Light direction and color
Light const& light = { normalized(Direction{1,1,1,0}), Pixel{255, 255, 255} };

// Phong illumination model's parameters
double Beta = 1.0; // Diffuse reflection component's weight
double Gamma = 1.0; // Specular reflection component's weight
int m = 37; // Specular reflection exponent parameter

Pixel object_color = Pixel{246, 153, 126}; // Pixel{134,55,100}; // Pixel{255, 255, 0};

// Translation and other model dependend code for the camera view plane.
// The values are for the bunny.obj model
const Vector3D object_relocation = Vector3D{0, 0.0,-0.4, 0};
const double viewplane_width = 0.2;
const double viewplane_height = 0.2;
const double plane_distance = -0.2; // This is the distance from the pin hole camera to the plane

// Creation of video from a sequence of images.
// ffmpeg -pattern_type glob -framerate 25 -i "*.ppm" output.mpeg

// ##############################################################################################################

int main(int argc, char *argv[])
{
    if(argc < 2) {
        cerr << "Please provide an object file" << endl;
        return -1;
    }

    string model_filename = string(argv[1]);
    Image img(800,800, Pixel{255,255,255});
    Model3D model(model_filename);

    model.addLight(light, 0.5);

    // If your object/model is not at the origin, uncomment and relocate the object
    // before any computation.
    // display_model.translate({vector to relocate object at origin})
    
    // First create a model with camera view plane.
    model.rotateY(y_axis_rotation);
    model.rotateX(x_axis_rotation);
    model.rotateZ(z_axis_rotation);
    model.translate(object_relocation);
    model.addCameraPlane(viewplane_width, viewplane_height, plane_distance);
    ofstream camera_view("camera_view_with_pinhole.obj");
    camera_view << "mtllib cube.mtl" << endl << model << endl;
    cout << "Camera View file created (with pin_hole): camera_view_with_pinhole.obj" << endl;

    {
        // Temporary model to analyse the projection set-up better
        auto display_model = model;
        display_model.translate(-1.0 * object_relocation);
        display_model.rotateZ(-z_axis_rotation);
        display_model.rotateX(-x_axis_rotation);
        display_model.rotateY(-y_axis_rotation);
        ofstream camera_view("camera_view.obj");
        camera_view << "mtllib cube.mtl" << endl << display_model << endl;
        cout << "Camera View file created: camera_view.obj" << endl;
    }

    auto camera_view_plane = model.get_camera_view_plane();
    auto light = model.getLight();

    //Remove [Debug] faces and vertices from the model
    model.removeLightFacesAndVertices();
    model.removeCameraPlaneFacesAndVertices();

    //First create bounding box for acceleration.
    model.createBoundingBox();
    capture_image(img, camera_view_plane, model, light, object_color, Beta, Gamma, m);

    string filename_str = "rendered_image.ppm";
    ofstream rendered_image(filename_str);//("test_" + filename_str + ".ppm");
    rendered_image << img;
    cout << "Image rendered in the file: " + filename_str << endl;
    
    return 0;
}
