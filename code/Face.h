#ifndef __FACE_H__
#define __FACE_H__

#include <iostream>
#include <string>
#include <sstream>
#include <vector>

using namespace std;

struct Point {
    int index = {};
    int texture_index = {};
    int normal_index = {};

    friend ostream& operator<<(ostream& os, const Point& p);
    friend istream& operator>>(istream& is, Point& p);
};

//Represents indices of points in the list of points.
//The indices start from 1 as per the obj document - should we use dummy at place 0?
struct Face
{
    Point p1;
    Point p2;
    Point p3;
    
    friend ostream& operator<<(ostream& os, const Face& f);
    friend istream& operator>>(istream& is, Face& f);
};

#endif //__FACE_H__
