#ifndef __RAY_H__
#define __RAY_H__

#include <limits>
#include "Vertex.h"
#include "Image.h"

using namespace std;

struct Intersection
{
    double t;
    double b1;
    double b2;
    Vector3D normal;
    
    bool isValid() const
    {
        double inf = numeric_limits<double>::infinity();
        if(t < inf and t > 0 and b1 >= 0 and b2 >= 0 and (b1+b2)<=1)
               return true;
        return false;
    }
    
    Intersection()
    {
        this->t = std::numeric_limits<double>::infinity();
        this->b1 = std::numeric_limits<double>::infinity();
        this->b2 = std::numeric_limits<double>::infinity();
    }
};

struct Triangle
{
    Vertex p0;
    Vertex p1;
    Vertex p2;
};

class Ray
{
public:
    Ray(Vertex const& o, Direction const& d);
    Vertex _origin;
    Direction _direction;
};

struct Light
{
    Direction _direction;
    Pixel     _color;
};

#endif //__RAY_H__
