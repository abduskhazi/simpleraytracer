#ifndef __MODEL3D_H__
#define __MODEL3D_H__

#include "Vertex.h"
#include "Face.h"
#include "Ray.h"
#include <vector>
#include <exception>
#include <fstream>

using namespace std;

struct CameraViewPlane{

    Vertex left_top;
    Vertex right_top;
    Vertex left_bottom;
    Vertex right_bottom;

    Vector3D get_horizontal_vector() const {
        return right_top - left_top;
    }

    Vector3D get_vertical_vector() const {
        return left_bottom - left_top;
    }
};

static constexpr double INF = std::numeric_limits<double>::infinity();
struct BoundingBox
{
    enum {
        Minimum = 0,
        Maximum = 1
    };

    double x[2] = {INF, -INF};
    double y[2] = {INF, -INF};
    double z[2] = {INF, -INF};

    void update(Vertex const& v)
    {
        if(x[Minimum] > v.x) x[Minimum] = v.x;
        if(y[Minimum] > v.y) y[Minimum] = v.y;
        if(z[Minimum] > v.z) z[Minimum] = v.z;
        if(x[Maximum] < v.x) x[Maximum] = v.x;
        if(y[Maximum] < v.y) y[Maximum] = v.y;
        if(z[Maximum] < v.z) z[Maximum] = v.z;
    }
};

class Model3D
{
    public:
    Model3D(){}
    Model3D(string const& obj_file_name);

    void addLight(Light const& light, double dist_adjustment);
    void removeLightFacesAndVertices();
    Light getLight() const { return _light; }

    void addCameraPlane(double width, double height, double dist);
    void removeCameraPlaneFacesAndVertices();
    CameraViewPlane get_camera_view_plane() const { return _camera_view_plane; }

    void createBoundingBox();
    bool intersectsBoundingBox(Ray const& r) const;
    vector<Intersection> getIntersectionList(Ray const& r) const;
    Model3D intersectedModel(Ray const& r) const;

    vector<Triangle> traingles() const;

    void applyTransformation(vector<vector<double>> const& Matrix);
    void translate(Vector3D const& vec);
    void rotateX(double angle);
    void rotateY(double angle);
    void rotateZ(double angle);

    friend ostream& operator<<(ostream& os, const Model3D& m);

    private:
        vector<Vertex> _vertices;
        vector<VertexNormal> _normals;
        vector<Face> _faces;

        BoundingBox _bbox;
        Light _light;

        // Caching Camera View plane for code readability
        CameraViewPlane _camera_view_plane;
};

#endif //__MODEL3D_H__
